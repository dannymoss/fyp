import React from 'react'
import { Switch, Route } from 'react-router-dom'
import HomePage from '@pages/HomePage'
import ExercisesPage from '@pages/ExercisesPage'
import ExercisePage from '@pages/ExercisePage'
import LoginPage from '@pages/LoginPage'
import TemplatePage from '@pages/TemplatePage'
import NotFoundPage from '@pages/NotFoundPage'

const createRoutes = () => {
  return (
    <TemplatePage>
      <Switch>
        <Route path='/' component={HomePage} exact />
        <Route path='/exercises' component={ExercisesPage} exact />
        <Route path='/exercises/:exercise' component={ExercisePage} />
        <Route path='/login' component={LoginPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </TemplatePage>
  )
}

export default createRoutes
