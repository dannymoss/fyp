import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ExerciseOverview.scss'

class ExerciseOverview extends Component {
  render () {
    return (
      <div styleName='item'>
        <div styleName='item-content'>
          <div styleName='item-title'>{this.props.title}</div>
          <div styleName='item-description'>
            <p>{this.props.description}</p>
          </div>
        </div>
        <div styleName='item-image'>
          <img src={this.props.image} alt='img' />
        </div>
      </div>
    )
  }
}

ExerciseOverview.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string
}

export default ExerciseOverview
