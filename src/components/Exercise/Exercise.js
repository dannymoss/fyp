import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Exercise.scss'

class MuscleGroups extends Component {
  renderMuscleGroup (muscleGroups) {
    return (
      <div>{muscleGroups.name}</div>
    )
  }
}

class Grips extends Component {
  renderGrips (grips) {
    return (
      <div>{grips.name}</div>
    )
  }
}

class Exercise extends Component {
  render () {
    const {MuscleGroups} = this.props
    const {Grips} = this.props
    return (
      <div styleName='wrapper'>
        <div>{this.props.name}</div>
        <div>{MuscleGroups.map(this.renderMuscleGroup)}</div>
        <div>{this.props.primaryMuscle}</div>
        <div>{this.propTypes.preperation}</div>
        <div>{this.propTypes.execution}</div>
        <div>{Grips.map(this.renderGrips)}</div>
        <div><img src={this.props.image} /></div>
        <div>{this.props.video}</div>
      </div>
    )
  }
}

Exercise.propTypes = {
  name: PropTypes.string.isRequired,
  muscleGroups: PropTypes.arrayOf(PropTypes.string).isRequired,
  primaryMuscle: PropTypes.string.isRequired,
  preperation: PropTypes.string.isRequired,
  execution: PropTypes.string.isRequired,
  grips: PropTypes.arrayOf(PropTypes.string).isRequired,
  image: PropTypes.string.isRequired,
  video: PropTypes.string
}

export default Exercise
