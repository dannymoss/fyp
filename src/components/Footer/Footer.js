import React, { Component } from 'react'
import './Footer.scss'
import SVGInline from 'svg-inline-react'
import facebook from '@icons/facebook.svg'
import instagram from '@icons/instagram.svg'
import linkedin from '@icons/linkedin.svg'
import twitter from '@icons/twitter.svg'

class Footer extends Component {
  render () {
    return (
      <div styleName='wrapper'>
        <div styleName='footer-wrapper'>
          <div styleName='top-container'>
            <div styleName='top-left-container'>
              <div styleName='info-logo'>
                {/* <img src=''/> */}
              </div>
              <div styleName='info-about'>
                <p styleName='info-about-details'>Danny Moss Final Year Project</p>
                <p styleName='info-about-details'>Nottingham Trent University</p>
                <p styleName='info-about-details info-about-other'>danny@moss.com | 07123456789</p>
              </div>
            </div>
            <div styleName='top-right-container'>
              <div styleName='social-container'>
                <a href='facebook.com' styleName='social-icon'>
                  <SVGInline src={facebook} />
                </a>
                <a href='twiter.com' styleName='social-icon'>
                  <SVGInline src={twitter} />
                </a>
                <a href='instagram.com' styleName='social-icon'>
                  <SVGInline src={instagram} />
                </a>
                <a href='linkedin.com' styleName='social-icon'>
                  <SVGInline src={linkedin} />
                </a>
              </div>
              <div styleName='info-copyright'>
                <p styleName='info-copyright-content'>© Danny Moss 2017</p>
                <p styleName='info-copyright-content'>Some other Information to go here</p>
              </div>
            </div>
          </div>
          <hr styleName='separator-line' />
          <div styleName='bottom-container'>
            <ul styleName='navigation'>
              <li styleName='navigation-content'><a href='#' styleName='navigation-item'>Other Link 1</a></li>
              <li styleName='navigation-content'><a href='#' styleName='navigation-item'>Other Link 2</a></li>
              <li styleName='navigation-content'><a href='#' styleName='navigation-item'>Other Link 3</a></li>
              <li styleName='navigation-content'><a href='#' styleName='navigation-item'>Other Link 4</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
