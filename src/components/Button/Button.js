import React, { Component } from 'react'
import './Button.scss'

class Button extends Component {
  render () {
    const { type } = this.props

    return (
      <button type={type} styleName='button' >
        <span styleName='focus-border'>
          <i />
        </span>
      </button>
    )
  }
}

Button.defaultProps = {
  type: 'text'
}

export default Button
