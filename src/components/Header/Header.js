import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import store from '@stores/NavigationStore'
import './Header.scss'

class Header extends Component {
  renderNavigationItems () {
    return store.items.map((value) => {
      return (
        <li styleName='navigation-content'>
          <Link to={value.url} styleName='navigation-item'>
            {value.title}
          </Link>
        </li>
      )
    })
  }

  render () {
    return (
      <div styleName='wrapper'>
        <div styleName='mobile-nav-button'>
          <input type='checkbox' id='spinner-form' />
        </div>
        <div styleName='main'>
          <div styleName='logo'>
            <a href='#' styleName='logo-link' />
          </div>
          <ul styleName='navigation'>
            {this.renderNavigationItems()}
          </ul>
        </div>
      </div>
    )
  }
}

export default Header
