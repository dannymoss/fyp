import React, { Component } from 'react'
import { observer } from 'mobx-react'
import './LoginPage.scss'
import { Link } from 'react-router-dom'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Spring from '@components/Spring'
import Input from '@components/Input'
import Button from '@components/Button'
import LoginStore from '@stores/LoginStore'

@observer
class LoginPage extends Component {
  renderUserMessage (isLoggedIn) {
    if (isLoggedIn) {
      return <p>Hello User</p>
    }

    return <p> Hello guest </p>
  }

  renderForm (isLoggedIn) {
    if (!isLoggedIn) {
      return (
        <form onSubmit={LoginStore.login}  styleName="form-wrapper">
          <Input type='text' placeholder='Username' />
          <Input type='password' placeholder='Password' />
          <Button type='submit' >
            Login
          </Button>
        </form>
      )
    }

    return (
      <a onClick={LoginStore.logout}>Logout</a>
    )
  }

  render () {
    return (
      <div>
        <Header />
        <Spring>
          {this.renderUserMessage(LoginStore.isLoggedIn)}
          {this.renderForm(LoginStore.isLoggedIn)}
        </Spring>
        <Footer />
      </div>
    )
  }
}

export default LoginPage
