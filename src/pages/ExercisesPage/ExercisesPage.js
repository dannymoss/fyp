import React, { Component } from 'react'
import './ExercisesPage.scss'
import { Link } from 'react-router-dom'
import store from '@stores/ExerciseStore'
import SVGInline from 'svg-inline-react'
import Silhouette from '@icons/silhouette.svg'

class ExercisesPage extends Component {
  renderCategories () {
    return store.categories.map((category) => {
      return (
        <div styleName='category'>
          <Link to={`/exercises/${category.url}`}>
            {category.title}
          </Link>
        </div>
      )
    })
  }

  render () {
    return (
      <div>
        <Link to='/'>
          HomePage
        </Link>
        <div styleName='wrapper'>
          <div styleName='categories-wrapper'>
            {this.renderCategories()}
          </div>
          <div styleName='silhouette-img-wrapper'>
            <h1>Silhouette to go here</h1>
            <div styleName='wrapper-body'>
              <SVGInline src={Silhouette} mapname='theBody' />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ExercisesPage
