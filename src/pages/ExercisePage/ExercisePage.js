import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import ExerciseOverview from '@components/ExerciseOverview'
import './ExercisePage.scss'

@observer
class ExercisePage extends Component {
  @observable data = null

  componentDidMount () {
    fetch(`/api/exercises/${this.props.match.params.exercise}`)
      .then(res => res.json())
      .then((res) => {
        console.log('data: ', res)
        this.data = res
      })
      .catch((error) => {
        console.warn('An error has occured', error)
      })
  }

  render () {
    if (!this.data) {
      return <span>loading...</span>
    }

    return (
      <div styleName='wrapper'>
        <div styleName='content'>
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
          <ExerciseOverview title='Exercise Name' description='Muscle, Groups' image='https://www.muscleandperformance.com/.image/c_limit%2Ccs_srgb%2Cq_80%2Cw_460/MTQ1MzY2OTYxOTM3MTk2NTk5/image-placeholder-title.webp' />
        </div>
        { this.data.message }
      </div>
    )
  }
}

export default ExercisePage
