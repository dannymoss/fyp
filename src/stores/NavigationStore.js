import { observable } from 'mobx'

class NavigationStore {
  items = [
    {
      title: 'HomePage',
      url: '/'
    },
    {
      title: 'Exercises',
      url: '/exercises'
    },
    {
      title: 'Workouts',
      url: '/workout'
    },
    {
      title: 'Account',
      url: '/login'
    }
  ]
}

const store = new NavigationStore()
export default store
