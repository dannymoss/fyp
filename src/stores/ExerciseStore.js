import { observable } from 'mobx'

class ExerciseStore {
  categories = [
    {
      title: 'Shoulders',
      url: 'shoulders'
    },
    {
      title: 'Traps',
      url: 'traps'
    },
    {
      title: 'Chest',
      url: 'chest'
    },
    {
      title: 'Triceps',
      url: 'triceps'
    },
    {
      title: 'Back',
      url: 'back'
    },
    {
      title: 'Lats',
      url: 'lats'
    },
    {
      title: 'Bicep',
      url: 'bicep'
    },
    {
      title: 'Abs',
      url: 'abs'
    },
    {
      title: 'Obliques',
      url: 'obliques'
    },
    {
      title: 'Glutes',
      url: 'glutes'
    },
    {
      title: 'Quads',
      url: 'quads'
    },
    {
      title: 'Hamstring',
      url: 'hamstring'
    },
    {
      title: 'Calves',
      url: 'calves'
    },
    {
      title: 'Cardio',
      url: 'cardio'
    }
  ]
}

const store = new ExerciseStore()
export default store
