import { observable } from 'mobx'

class LoginStore {
  @observable isLoggedIn = false
  @observable data = {
    username: '',
    password: ''
  }

  login = (event) => {
    // fetch('/login', {
    //   method: 'post'
    // })
    event.preventDefault()
    this.isLoggedIn = true
    console.log('loggedin', this.isLoggedIn)
  }

  logout =  (event) => {
    event.preventDefault()
    this.isLoggedIn = false
  }
}

const store = new LoginStore()
export default store
