import express from 'express'
import exercises from './api/exercises'

const router = express.Router()

router.get('/api/exercises/:exercise?', exercises)

export default router
