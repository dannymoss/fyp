const exercises = (req, res) => {
  if (req.params.exercise) {
    res.send({
      message: `This will return ${req.params.exercise} exercises.`
    })
  } else {
    res.send({
      message: 'This will return all of the exercuses'
    })
  }
}

export default exercises
