import express from 'express'
import bodyParser from 'body-parser'
import template from './template'
import path from 'path'
import router from './router'

const app = express()

app.use(bodyParser.json())
app.use(express.static(path.join(process.argv[1], '../')))
app.use(router)

if (process.env.NODE_ENV === 'local') {
  const webpack = require('./config/webpack')

  app.use(webpack.webpackDev)
  app.use(webpack.webpackHot)
}

app.get('*', (req, res) => {
  res.send(template)
})

app.listen(8000, 'localhost', console.warn)
